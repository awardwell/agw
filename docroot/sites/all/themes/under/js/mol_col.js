/**
 * Created by Awardwell on 1/4/14.
 */


(function($){

  // picture model
  ugwf.models.picture = Backbone.Model.extend({
    defaults : {
      'url' : 'http://placekitten.com/200/300',
      'alt' : 'picture',
      'title' : 'picture'
    }
  });

  //song model
  ugwf.models.song = Backbone.Model.extend({
    defaults : {
      'url' : 'song',
      'title' : 'song',
      'name' : 'song',
      'type' : 'audio',
      'filemime' : 'audio/mpeg'
    }
  });

  // Album Picture model
  ugwf.collections.pictures = Backbone.Collection.extend({
     model : ugwf.models.picture
  });

  // Album Song model
  ugwf.collections.songs = Backbone.Collection.extend({
    model : ugwf.models.song
  });


  ugwf.models.album = Backbone.Model.extend({
      idAttribute: 'nid',

      initialize : function(){
        var pictures = this.get('pictures');
        var songs = this.get('songs');
        var p_col = [];
        var s_col = [];
        // _.each binds iterator to the context you pass in v is the item in the list
        _.each(pictures, function(v, k, list){
          // we have to pass an array(of objects) to the collection
          p_col.push(v);
        }, pictures);
        _.each(songs, function(v, k, list){
          // we have to pass an array(of objects) to the collection
          s_col.push(v);
        }, songs);
        this.set('pictures', new ugwf.collections.pictures(p_col));
        this.set('songs', new ugwf.collections.songs(s_col));
      },

      defaults : {
        'nid' : 0,
        'title' : 'album',
        'body' : 'favorite album',
        'songs' : [],
        'pictures' : []
      }
  }
  );

  ugwf.collections.albums = Backbone.Collection.extend({
    model : ugwf.models.album,

    initialize : function(){}

  });


  ugwf.model_init = function(){
    ugwf.new_albums = new ugwf.collections.albums(Drupal.settings.albums);
  };

  $(document).ready(function(){

      ugwf.model_init();
    }
  );
})(jQuery);