/**
 * Created by Awardwell on 1/4/14.
 */


(function($){
  // In order to do a router you need three things
  //1) The extension from the backbone router
  //2) Click event that calls the switch .navigate on the router
  //3) The function that does something in reaction to the router
  ugwf.app_router = Backbone.Router.extend({
    //Define the routes *actions (matches anything) where as reading matches reading
    routes: {
      //declare special paths
      'album/:name': 'albumDisplay',
      // have the defualt router happening
      'home' : 'homeDisplay',
      '*actions': 'defaultRoute'
    },

    trackEvent : function(url, title){
      ga('send', 'pageview', {
        'page': url,
        'title': title
      });
    },

    // callback function for displaying a select album
    albumDisplay : function(name) {
      $('body').addClass('page-album').removeClass('home');
      $('.opening-vid #ytplayer').detach();
//      ugwf.social.c_view.$el.css('opacity', 0).hide();
      $('.active').removeClass('active');
      var id = $('.'+name).data('id');
      $('.'+name).addClass('active');
      var album = ugwf.new_albums.findWhere({nid : id.toString()});
      document.title = 'Underground Wildfires | '+album.get('name');
      // Tracking
      this.trackEvent(window.location.pathname, document.title);
      if(ugwf.album_view){
        ugwf.album_view.cleanup();
      }
      ugwf.album_view = new ugwf.albumView({model: album});
    },

    homeDisplay : function(name){
      ugwf.openingVid();
      $('body').removeClass('page-album').addClass('home');
      document.title = 'Underground Wildfires | '+name;
//      ugwf.social.c_view.$el.show().css('opacity', 1);
      ugwf.videoResizr();
      // Tracking
      this.trackEvent(window.location.pathname, document.title);
    }

  });

  ugwf.backboneRouter = function(event) {
    // get the target of the clicked element from ^^^^
    href = $(event.currentTarget).attr('href');

    // chain 'or's for other black list routes
    passThrough = href.indexOf('sign_out') >= 0;

    // Allow shift+click for new tabs, etc.
    if (!passThrough && !event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
      event.preventDefault();
    }

    //Remove leading slashes and hash bangs (backward compatablility)
    url = href.replace(/^\//, '').replace('\#\!\/', '');

    // Instruct Backbone to trigger routing events thus the trigger true ie switches the route then
    // triggers the event that it matches
//    if($('.not-logged-in').length > 0){
      ugwf_router.navigate(url, { trigger: true });
//    }
    return false;
  };


  // function that begins or enables routing
  ugwf.routerInit = function(){
    // this refers to ugwf?
    //init a bunch of the variables needed
    ugwf_router = new ugwf.app_router;

    // enable pushState
    Backbone.history.start({ pushState: true });
    // bind to click event to DOM element
//   $.when.apply($, ugwf.deferreds).then(function() {
      $("a[href^='/']").on('click', function(event) {
      ugwf.backboneRouter(event);
      });
//    });
  };

  $(document).ready(function(){
    ugwf.routerInit();
  });
})(jQuery);