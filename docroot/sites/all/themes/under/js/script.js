/**
 * Created with JetBrains PhpStorm.
 * User: Awardwell
 * Date: 11/12/13
 * Time: 9:59 PM
 * To change this template use File | Settings | File Templates.
 */

//<script type="text/template" id="item-template"></script>
//<script type="text/template" id="stats-template"></script>
//  <script src="js/lib/jquery.min.js"></script>
//  <script src="js/lib/underscore-min.js"></script>
//  <script src="js/lib/backbone-min.js"></script>
//  <script src="js/lib/backbone.localStorage.js"></script>
//  <script src="js/models/todo.js"></script>
//  <script src="js/collections/todos.js"></script>
//  <script src="js/views/todos.js"></script>
//  <script src="js/views/app.js"></script>
//  <script src="js/routers/router.js"></script>
//  <script src="js/app.js"></script>
(function($) {
  // This is going to be the Social object comprised of the model view and helper functions
  //init the object
  ugwf = {};
  ugwf.models = {};
  ugwf.collections = {};
  ugwf.views = {};

  ugwf.init = function() {
    // change wrappers to {{Mustache}}
    _.templateSettings = {
      interpolate: /\{\{(.+?)\}\}/g
    };
    $.support.cors = true;
    // create a deffered object
    ugwf.deferreds = [];
  };

  ugwf.social = {};

  ugwf.social.model = Backbone.Model.extend({
    defaults: {
      message: '',
      link: ''
    },

    initialize: function(){
      if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
      }
      this.fetch();
    },

    url: window.location.origin+'/data/fb_feed',

    parse : function(results){
      var first = _.first(results);
      this.set('message', first.message);
      this.set('link', first.link);
    }
  });

  ugwf.social.view = Backbone.View.extend({
    //this initiation fuction
    el: '.social_message',
    //calling the rendering function on init
    initialize: function() {
      this.model.on('change', this.render, this);
//      this.render();
    },

    events : {
      'complete' : 'fadeIt'
    },

    // we are going to give the image tag above the attributes from the image etc.
    render: function() {
      var message = this.model.get('message');
      var date = this.model.get('date');
      var link = this.model.get('link');
      var social_template = _.template($("#social_message").html(), {message : message, link : link});
      this.$el.html(social_template);
      this.on('complete', this.fadeIt(), this);
    },

    fadeIt : function(e){
      this.$el.css('opacity', 1);
    }
  });

  ugwf.social_init = function() {
    ugwf.social.current = new ugwf.social.model();
    ugwf.social.c_view = new ugwf.social.view({model : ugwf.social.current});
    // read the json and parse into our current backbone model
  };

  ugwf.openingVid = function(){
//    var video_id = 'M6o0DpbCxa8';
    var video_id = 'hJTAbkBbsYc';
    var video_template = _.template($("#vid_template").html(), {video_id : video_id});
    $('.opening-vid').html(video_template);
  };

  ugwf.jPlayerInit = function() {
    $('#jPlayer').jPlayer({
      ready: function() {

      },
      supplied: "mp3",
      timeupdate: function(e) {
      $('.progress-bar').css('width', e.jPlayer.status.currentPercentAbsolute+'%');
      },
      ended: function(e){
        var next_song = $('.active-song').next();
        $('.active-song').removeClass('active-song').removeClass('icon-headphones-1');
        next_song.addClass('active-song').addClass('icon-headphones-1');
        var song_data = next_song.data('song');
        var song_title = next_song.text();
        var vars = {
          category : 'music',
          action : 'next_song',
          label : song_title
        };
        ugwf.trackEvent(vars);
        $('.song-title').html(song_title);
        $('#jPlayer').jPlayer('setMedia', {
          'mp3' : song_data
        }).jPlayer('play');
      }
    });
  };

  /**
   * Resizes an iframe.video.resizr to be the width of the container,
   * and sets the scale to whatever you set in the data attribute.
   *
   * Scale should be entered as data-width=16 and data-height=9.
   *
   * Example:
   *  <iframe class="video resizr" data-width=4 data-height=3 src=""></iframe>
   *  This will set the video to a 4:3 ratio.
   */
  ugwf.videoResizr = function() {
    $('iframe.video.resizr').each(function(i) {
      // Set width based on parent container.
      var width = $(this).parent().width();
      // Get ratio info from the data attributes.
      var scale_width = $(this).data('width');
      var scale_height = $(this).data('height');
      // Get new height calculation.
      var height = width * scale_height / scale_width;
      // Set the new width/height values.
      $(this).width(width).height(height);
    });
  };

  /**
   * Send an event to Google Analytics.
   *
   * @param array vars
   *  An array of variables to send to Google Analytics.
   *  Valid vars include: category, label, action, value.
   */

  ugwf.trackEvent = function(vars) {
    if (typeof ga == 'function') {
      ga('send', {
        hitType: 'event',
        eventCategory: vars.category,
        eventAction: vars.action,
        eventLabel: vars.label,
        eventValue: vars.value
      });
    }
  }


  ugwf.stickyHeader = function(){
    $('#block-ugwf-blocks-ugwf-header').waypoint('sticky', {
//    $('.site-heading').waypoint('sticky', {
      stuckClass: 'stuck-header',
      offest: 50
    });
  };

  $(document).ready(function() {
    ugwf.init();
//    ugwf.social_init();
    ugwf.openingVid();
    ugwf.jPlayerInit();
//    ugwf.stickyHeader();
    if ($('iframe.video.resizr').length > 0) {
      // Start video resizer magic.
      ugwf.videoResizr();
      // On window resize, we want to change the video size, ya know?
      $(window).resize(function() {
        window.clearTimeout(ugwf.resizr);
        ugwf.resizr = window.setTimeout(function() {
          ugwf.videoResizr();
        }, 250);
      })
    }
  });

})(jQuery);