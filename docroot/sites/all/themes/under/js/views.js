/**
 * Created by Awardwell on 1/4/14.
 */


(function($){

  ugwf.albumView = Backbone.View.extend({

    // change wrappers to {{Mustache}}
//    _.templateSettings = {
//    interpolate: /\{\{(.+?)\}\}/g
//    };

    el: '.album_container',

    template : $('#album_template').html(),

    events: {
      'click div.icon-right-dir' : 'expandList'
    },

    initialize : function(){
//      _.bindAll(this, 'cleanup');
      this.render();
    },

    render: function (){
      var title = this.model.get('title');
      var body = this.model.get('body');
      var raw_pictures = this.model.get('pictures');
      var raw_songs = this.model.get('songs');
      var songs = new ugwf.songsView({collection : raw_songs});
      var pictures = new ugwf.picturesView({collection : raw_pictures});

      var album_temp = _.template($("#album_template").html(), {title: title, body: body});

      this.$el.html(album_temp);
      this.$('.songs-container').append(songs.el);
      this.$('.pictures-container').append(pictures.el);
    },

    expandList : function(e){
      var collapsed = this.$('.songs-container').hasClass('collapsed');
      if(collapsed){
        this.$('.songs-container').removeClass('collapsed');
      } else {
        this.$('.songs-container').addClass('collapsed');
      }
    },

    cleanup: function() {
      this.undelegateEvents();
      $(this.el).empty();
    }

  });


  ugwf.songsView = Backbone.View.extend({
    tagName : 'ul',
    className : 'songs-list',
    initialize : function(){
      this.collection.on('change', this.render(), this);
//      this.render();
    },

    events: {
      'click .song_item' : 'loadSong'
    },

    render : function (){
      var i = 0;
      this.collection.forEach(function(item){
        var filemime = item.get('filemime');
        var type = item.get('type');
        var url = item.get('url');
        var name = item.get('name');
        var title = item.get('title');
//        var activeClass = '';
//        if(i == 0){
//          var activeClass = 'active-song';
//          i = 1;
//        }
//        item.set('state', activeClass);
        var song_template = _.template($('#songs_template').html(), {title : title, url : url});

        this.$el.append(song_template);
      }, this);
    },

    loadSong : function (e){
      $('.active-song').removeClass('active-song').removeClass('icon-headphones-1');
      var cl_el = e.currentTarget;
      var title = cl_el.innerText;
      $('.song-title').html(title);
      var vars = {
        category : 'music',
        action : 'loaded_song',
        label : title
      };
      ugwf.trackEvent(vars);
      cl_el.classList.add('active-song');
      cl_el.classList.add('icon-headphones-1');
      var song_route = cl_el.dataset['song'];
      $('#jPlayer').jPlayer('setMedia',{
        mp3: song_route
      }).jPlayer('play');
    }

//    nextSong : function(currentSong){
//
//    }
  });


  ugwf.picturesView = Backbone.View.extend({
    className : 'pictures-slider',
    initialize : function(){
      this.collection.on('change', this.render(), this);
    },

    render : function (){
      var length = this.collection.length;
      console.log(length);
      var width = 1100 * length;
      this.collection.forEach(function(item){
        var url = item.get('url');
        var alt = item.get('alt');
        var title = item.get('title');
        var pic_template = _.template($('#pictures_template').html(), {title : title, alt: alt, url : url});
        this.$el.append(pic_template);
      }, this);
      this.$el.width(width);
    }
  });


})(jQuery);