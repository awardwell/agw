<div class = "block block--menu">
  <ul class = "album_menu">
     <li class = "album_menu__item music_heading"><span class = "title-music">
     <object type="image/svg+xml" data="<?php print url(drupal_get_path('theme', 'under').'/svgs/UGWFlogoNEW.svg', array('absolute' => TRUE))?>">Your browser does not support SVG</object>
     <span>
     </li>
    <?php foreach ($menu_titles as $key => $title): ?>
    <li class = "album_menu__item <?php print $title['url']; ?>" data-id = "<?php print $key; ?>">
      <a href="/album/<?php print $title['url']; ?>"><?php print $title['title']; ?></a>
    </li>
    <?php endforeach; ?>
  </ul>
</div>