<div class = "footer">
  <div class = "info">
    <ul class = "info_list">
      <li class = "info_list__item">
        Underground Wildfires is Aaron Wardwell.
      </li>
      <li class = "info_list__item">
        <a href="http://undergroundwildfires.bandcamp.com/" target="_blank">You can download his music here.</a>
      </li>
      <li class = "info_list__item">
        <a href="mailto:aaron.wardwell@gordon.edu">Book Underground Wildfires</a>
      </li>
    </ul>
  </div>
  <div class = "sharing">
    <div class = "sharing__item">
      <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FUnderground-Wildfires%2F173281819371925&amp;width=10&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21&amp;appId=545954452149670" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:135px; height:21px;" allowTransparency="true"></iframe>
    </div>
  </div>
</div>
