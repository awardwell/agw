<script type = "text/html" id= "social_message" >
    <a href="{{link}}">{{message}}</a>
</script>
<script type = "text/html" id= "songs_template" >
  <li data-song ="{{url}}" class = "song_item">
    <span>{{title}}</span>
  </li>
</script>
<script type = "text/html" id= "pictures_template" >
  <div class = "album_picture">
    <img title="{{title}}" alt="{{alt}}" src="{{url}}" />
  </div>
</script>
<script type = "text/html" id= "album_template" >
  <div class = "album_info">
  <div class = "album_title">
    <span>{{title}}</span>
  </div>
  <div class = "album_body">
    <span>{{body}}</span>
  </div>
    <div href="" class = "icon-right-dir"></div>
 </div>
  <div class = "songs-container collapsed">
  </div>
  <div class = "pictures-container">
  </div>
</script>
<script type="text/html" id = "vid_template">
  <iframe id="ytplayer" type="text/html" data-width="16" data-height="9" class = "video resizr"
          src="https://www.youtube.com/embed/{{video_id}}?autoplay=1&controls=0&enablejsapi=1&modestbranding=1&autohide=1&color=white"
          frameborder="0" allowfullscreen>
  </iframe>
</script>
<div class = "present-container">
  <div class = "social_message">
  </div>
  <div class = "opening-vid">
  </div>
  <div class = "album_container">
  </div>
  <div id = "jPlayer"></div>
  <div id="jp_container_1" class = 'player-box'>
  <a href="#" class="jp-play icon-play"></a>
  <a href="#" class="jp-pause icon-pause"></a>
  <div class = "progress-box">
     <span class = 'song-title'></span>
     <div class = "progress-bar"></div>
  </div>
  </div>
</div>
